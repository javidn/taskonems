package az.ingress.task1.Controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MyClass {

    @GetMapping("/print")
    public String printHello(){
        return "Hello from Task1";
    }

}
